<?php

class Logger 
{
    private $fHandler = null;
    private $logFile = "";
    
    public function __construct()
    {
        $this->logFile = LOG_FILE;
    }
    
     /**
     * Write a log level line into the log file
     * */
    public function log($string) {
        $this->write($string, "LOG");
        return $this;
    }

    /**
     * Write a warn level line into the log file
     * */
    public function warn($string) {
        $this->write($string, "WARN");
    }

    /**
     * Write an error level line into the log file
     * */
    public function error($string) {
        $this->write($string, "ERROR");
    }

    private function open() {
        if (!$this->fHandler = fopen($this->logFile, "a")) {
            return false;
        }
        return $this->fHandler;
    }

    private function close() {
        fclose($this->fHandler);
    }

    /**
     * Write line into log file
     * */
    private function write($string, $level = "LOG") {
        $string = date("Ymd - H:i:s", time()) . " [" . $level . "]  " . $string . "\n";
        if (!$handle = $this->open()) {
            echo "Can't write into logfile";
        }
        fwrite($handle, $string);
        echo "<pre>" . $string . "</pre>";
        $this->close();
    }

}
