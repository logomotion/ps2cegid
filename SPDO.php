<?php

/**
* 
*/
class SPDO
{
    const DSN = "mysql:dbname=db_sandbox;host:localhost";
    const DB_USER = "logomotion";
    const DB_PWD = "XoHo2W7H";
    /**
     * PDO class instance
     * @var PDO
     * @access private
     */
    private $PDOInstance = null;
    
    /**
     * SPDO class instance
     * @var SPDO
     * @access private
     @static
     */
    private static $instance = null;
    
    private $logger = null;
    
    
  private function __construct()
  {
    $this->PDOInstance = new PDO(self::DSN,self::DB_USER,self::DB_PWD);
    $this->PDOInstance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->logger = new Logger();
  }
  
  public static function getInstance()
  {
  	if(is_null(self::$instance)){
            self::$instance = new SPDO();
        }
        return self::$instance;
  }
  
  public function __call($name, $arguments) {
      return self::$instance->$name(implode(", ", $arguments));
  }
  
  public function prepare($statement)
  {
      $ret = $this->PDOInstance->prepare($statement);
      $error = $this->PDOInstance->errorInfo();
      if($error[0] != "00000"){
         $this->logger->error("Query error : " . $error[2]);
      }
      return $ret;
  }
  
  
  /**
   * Exports a table to a file
   */
  public function backUpTable($tableName){
    $backupString = "-- ".$tableName."\n";
    $backupString .= "-- STRUCTURE \n";
    $query = $this->prepare("SHOW CREATE TABLE ".$tableName);
    $query->execute();
    $result = $query->fetch();
    $backupString .= $result["Create Table"];
    $backupString .= "\n\n -- DATAS \n";
    $query = $this->prepare("SELECT * FROM ".$tableName);
    $query->execute();
    $result = $query->fetchAll();
    foreach ($result as $row) {
      $backupString .= "INSERT INTO ".$tableName. " VALUES(";
      $i = 1;
      foreach ($row as $value) {
          $backupString .= "'".$value."'";
          if(count($row) > $i) $backupString .= ", ";
          $i++;
        }
      $backupString .= ");\n";
    }
    
    $fileName = __DIR__."/backups/".$tableName."_".date("Ymd-His",time()).".sql";
    return file_put_contents($fileName, $backupString);
  }

  public function placeHolders($text, $count=0, $separator=","){
  	$result = array();
  	if($count > 0){
  		for ($x=0; $x < $count; $x++) { 
  			$result[] = $text;
  		}
  	}
  	return implode($separator,$result);
  }
}

